import requests, os
from bs4 import BeautifulSoup

url = ""


def getImages():
    sweet = "https://wiki.guildwars.com/wiki/Sweet"
    drunk = "https://wiki.guildwars.com/wiki/Alcohol"
    party = "https://wiki.guildwars.com/wiki/Festive_item"

    def menu():
        print(os.getcwd())
        print("Which title images to get?")
        print("[1] Drunk")
        print("[2] Party")
        print("[3] Sweet")
        print("[Q] Exit the program")

    menu()
    answer = input("\nPlease select what you want to do.\n >> ")

    while answer != "Q" or answer != "q":

        if answer == "1":
            url = drunk
            urltest = requests.get(url, headers={"User-Agent": "Mozilla/5.0"})
            # print(urltest.status_code)
            if not urltest.status_code == 200:  # html status code 200 means connection successful
                quit()  # Quit the program if url status is not 200
            else:
                print('Connection successful')
            soup = BeautifulSoup(urltest.text, "html.parser")
            table = soup.find('table')
            images = table.find_all("img")
            # print(images) # Debug check if used correct html tag above
            resolved = []
            for image in images:
                src = image.get("src")
                resolved.append(requests.compat.urljoin(url, src))
            # print(resolved) # Debug check if the image path looks correct
            abspath = os.path.abspath(__file__)  # Set working directory
            dname = os.path.dirname(abspath)
            os.chdir(dname)
            if not os.path.exists(
                    r"images/drunk/"):  # Check for images directory
                os.makedirs(r"images/drunk/")  # Make images directory
            for image in resolved:  # Save every image found with img tag
                print(image)  # Print the image address
                webs = requests.get(image)
                open(r"images/drunk/" + image.split("/")[-1],
                     "wb").write(webs.content)

        elif answer == "2":
            url = party
            urltest = requests.get(url, headers={"User-Agent": "Mozilla/5.0"})
            # print(urltest.status_code)
            if not urltest.status_code == 200:  # Url status 200 = working connection
                quit()  # Quit the program if url status is not 200
            else:
                print('Connection successful')
            soup = BeautifulSoup(urltest.text, "html.parser")
            table = soup.find('table')
            images = table.find_all("img")
            # print(images) # Debug check if used correct html tag above
            resolved = []
            for image in images:
                src = image.get("src")
                resolved.append(requests.compat.urljoin(url, src))
            # print(resolved) # Debug check if the image path looks correct
            abspath = os.path.abspath(__file__)  # Set working directory
            dname = os.path.dirname(abspath)
            os.chdir(dname)
            if not os.path.exists(
                    r"images/party/"):  # Check for images directory
                os.makedirs(r"images/party/")  # Make images directory
            for image in resolved:  # Save every image found with img tag
                print(image)  # Print the image address
                webs = requests.get(image)
                open(r"images/party/" + image.split("/")[-1],
                     "wb").write(webs.content)

        elif answer == "3":
            url = sweet
            urltest = requests.get(url, headers={"User-Agent": "Mozilla/5.0"})
            # print(urltest.status_code)
            if not urltest.status_code == 200:  # Url status 200 = working connection
                quit()  # Quit the program if url status is not 200
            else:
                print('Connection successful')
            soup = BeautifulSoup(urltest.text, "html.parser")
            table = soup.find('table')
            images = table.find_all("img")
            # print(images) # Debug check if used correct html tag above
            resolved = []
            for image in images:
                src = image.get("src")
                resolved.append(requests.compat.urljoin(url, src))
            # print(resolved)  # Debug check if the image path looks correct
            abspath = os.path.abspath(__file__)  # Set working directory
            dname = os.path.dirname(abspath)
            os.chdir(dname)
            if not os.path.exists(
                    r"images/sweet/"):  # Check for images directory
                os.makedirs(r"images/sweet/")  # Make images directory
            for image in resolved:  # Save every image found with img tag
                print(image)  # Print the image address
                webs = requests.get(image)
                open(r"images/sweet/" + image.split("/")[-1],
                     "wb").write(webs.content)

        elif answer == "Q" or "q":
            quit()

        else:
            print("Invalid option!")

        print()
        menu()
        answer = input("\nPlease select what you want to do.\n >> ")


pass

getImages()
